<h1> MezaSim Web Manager</h1>
<br><br><br>

<h2>General aspects</h2>

<br><br>

<h3>Introduction</h3>

Welcome to the MezaSim Web Manager.
<br>
To access to the website, you have to download the project. Then, start go in PackageMEZA and launch the launcher (msl.py as a script for the moment).
In the browser bar, enter http://localhost:8080/Meza_Manager/public/index.php .<br/>

<h3>On the site</h3>

Now, you are on the site. You can navigate with the Menu.
 To return to the first page, you can click on the name of the grid.
 
<h3>Connection</h3>
 
 The Login button allows you to connect yourself. You have to enter your mail address and your password to be connected. You also need an account to do this.<br/><br/><ul>
 - You can create an account in the "Registration" page which can be found in the Menu in the Users section. You must enter a first name and a last name for your avatar. You have to enter your mail address too. Then you can select an avatar
 (the name is just the name of the model). Finally, you have to enter and confirm a password with letters and numbers. 
 <br/><br/>
 - You can enter your real first name and your real last name and then click on Automatic Generation to generate your avatar name with your real name. Your mail will also be completed with first.last@ . You have to complete your mail to be registered.<br/><br/>
  - There is a default admin account. To connect yourself with this account, you have to enter this mail address: "admin@test.ex" <br/>and this password: "Opensim1". For more security, it's advisable to create your own account, then to log in with the default admin account and add your own account to the Admin's group by editing it, and finally to delete this default admin account.
  </ul>
  <br>
 
<h3>Groups</h3>
 
 You can create groups for the site to manage the authorizations. The level of a group indicate the authorization level.
 An higher level means a higher responsibility.<br/><br/><ul>
 - Users with high level can manage some aspect of other users with a lower level.<br/><br/>
 - The maximum level of a new group is 999.999. The Admin group is the only one with the level 1.000.000 . Don't delete this group you want to keep admin powers!</ul>
 
<h3>User Level</h3>
 
 This level indicate how much you understand the OpenSim option. If you don't understand what an option is doing, don't touch it !<br/><br/><ul>
 - beginner : You just want to create a grid, some regions, etc... without complicated options<br/><br/>
 - advanced : You want to create more personalized things. You have access to more options but no technical option.<br/><br/>
 - expert : You want to manage totally your creations. You have access to all of the options.
 </ul>
<h3>Avatars</h3>

This Web Manager allow you to upload avatars. To see the available avatars, go to the Menu and click on Avatars in the section Users.
You can upload avatars in the tab "Create an avatar". An avatar must have an unique name, a .dae file (the avatar file) and a thumbnail.
The thumbnail must be in the png format and must be a 320x320 px maximum image.
<br>
When you create an avatar, it is classified as a class C avatar. The class can be modified after by someone with an higher group level. The classes are:
<br/><br/><ul>- TP : All public, no one can be hurt by the appearance of the avatar.<br/><br/>
- L : Limited, the appearance of the avatar can hurt the sensibility on the most fragile.<br/><br/>
- A : Adult, this avatar is reserved for the adults and should not be use with children.<br/><br/>
- C : To be classified, this avatar has not been classified. An admin must classify this avatar before its use.</ul>

<br>

<h2>Grids<h2/>

<h3>Create a grid</h3>

You can access to the Grid part of the manager by clicking on Grid in the Splash screen section of the menu.
After that, you arrive on a page that expose the existing grids. We'll talk about that later.
To create a grid, click on the New Grid tab. To create a grid, you have to upload an image. This image will be the logo of the grid.
You also have to enter a new name for the grid. You have to enter the URL address of your grid to create it. Then, you click on Save and your grid will be created for the website.

<h3>Grid list</h3>

The grid list allows you to see the existing grids and if you have a sufficient group level, you can edit a grid.
You can change the logo of the grid and its name. You can also delete a grid.

<br>

<h2>Splash Screen</h2>

<h3>Select a grid</h3>

To see or modify a splash screen, you must select a grid. To do that, go to the Grid part of the manager.
When you see the list of the grids, you can select a grid by clicking the select button. It'll become a leave button.
Now you have selected a grid for the manager.

<h3>The splash screen</h3>

You access to the splash screen of the selected grid by clicking on Splash screen in the Splash screen section of the menu.
You will see the splash screen without the base aspect of the site.<br>
If the page doesn't load properly, you can reload the page and it would be display well.

<h3>Change the content of the splash screen</h3>

To change what appears in the splash screen, you can click on Settings in the Splash screen section of the menu.
You'll see a form and couples of tabs. Each field of each tab correspond to a setting.
<br/><br/><ul>- Settings:<br/><br/>
    - You can display the list of the regions in the grid<br/><br/>
    - You can display the flash news of the grid. For more details, see the Flash News section below.<br/><br/>
    - You can select the effect of the backgroud. It can be a loop of various images or an image wich corespond to the time of the day<br/><br/>
    - You can display the social networks block that display some information from social networks<br/><br/>
    - You can display a box to permit to a visitor to register his self on the splash screen<br/><br/>
- Status:<br/><br/>
    - You can display the status block<br/><br/>
    - You can select the status of the grid (Online or Offline)<br/><br/>
    - You can display some statistics about the grid in this block<br/><br/>
- Message:<br/><br/>
    - You can display the general message block<br/><br/>
    - You can enter the title of this message<br/><br/>
    - You can enter the message in the html format<br/><br/>
- Status message:<br/><br/>
    - You can display the status message block<br/><br/>
    - You can enter the title of this message<br/><br/>
    - You can select the color of the border of the block<br/><br/>
    - You can enter the message in the text format<br/><br/>
- Regions: You can add or remove regions to the grid. You can also order the regions. This order will be respected when the splash screen will be display<br/><br/>
- Loop: You can add and delete some images to display in the background of the splash screen if you have select Loop background in the first form<br/><br/>
- Time of the day: You can modify the images to display in the background of the splash screen if you have selected Time of the Day in the first form
</ul>
<h3>Modify the layout of the splash screen</h3>

You can modify the layout of the splash screen by clicking on Layout in the Splash screen section of the Menu.
You'll see a form. In this form, you can select where a box will be display in the splash screen.
All of the numbers are percentages of the screen.<br>
If you select Upbar or Downbar in the first part, your block will be displayed on the bars of the splash screen.
The top bar is a bar with a text that move continuously to the left. The bottom bar is just a bar with the text written in it.
If you select a bar placement, you don't have to fill the fields about coordinates and size.
But if you select the Content placement, you must enter coordinates and size. If the numbers are not correct, the manager will rectify the coordinates and the size to fit in the screen.
<br>
The manager will not verify if the blocks are in the same place so be careful if you don't want to see blocks on top of each others.
<br>
When you click on "Save and view", the placements will be registered and you will see the splash screen.

<h3>Flash News</h3>

You can manage the flash news in the Flash News tab in the Splash screen section of the menu.
You'll see a list of the news. You can delete a news by clicking on the delete button near the new.
<br>
A new has a title, a status, an expiration date and a content. To create a new news, you can click on New Flash New and fill the form and click on Save.
If a flash new pass the expiration date, its status become "expired" and it will not be display in the splash screen.

<br/>
<h2>Map</h2>

When you have selected a grid, you can see a map of it. To see the map, click on Map in the Map section of the Menu.<br/>
You'll see a map with a blue background and some rectangles representing the regions.
You can see on each region the coordinates of it and its name.

<br>

<h2>  Ini files </h2>

<br>

<h3>What is this?</h3>

Ini files allow you to create an OpenSim simulation. You have to create such files to have a functional grid.
The pages are in the "Files Generation" section in the Menu.

<h3>Region</h3>

You have to set up regions for your simulation. In the page "Regions" you can create regions, edit them and link them together to generate a Regions.ini file.
To create a region, go to the "Create Region" tab. You should find a form. You have to complete this form and you can modify the options if you want (and if you know what you do).
Each region must have an unique name and a unique position. You can edit any region managed by someone with a lower group level than yours and of course, regions managed by you.
<br>
After create and edit region, you can create Regions.ini files. To do that, go to the "Generate ini files" tab.<br/><br/><ul>
 - Here, you can link regions. To do that, go to the first part of the page and enter a new name for your data. Then select which region you want to have on the Region.ini file. And finally click on Link.
Your data will appear below in the second part of the page.<br/><br/>
 - You can also generate a previously created Regions.ini file. The data are in the second part of the page. You just have to select the ini file you want and click on Generate. Your file will be generated and stored in the ini/Region folder.
 You will find a folder with the name of the data you have selected. In this folder, there is the Regions.ini file.</ul>
 
<h3>Opensim / MyWorld</h3>

You have to create an Opensim.ini file and a MyWorld.ini file to launch your grid. You can create it by clicking on Opensim/MyWorld in Files generation section of the menu. Only admin users can create or edit such files.<br/>
To create these files, you have to go on the Create OpenSim/MyWorld tab. You'll see a form. You have to fill all the required fields (with flags). Warning: the Estate name and the Grid name must be unique. When you have filled the form, click on Save at the bottom of the page. You can also edit these files by clicking on Edit in the Datas OpenSim/MyWorld section <br/>
Now, the OpenSim data for your Opensim.ini and the MyWorld data for your MyWorld.ini are registered in the database. You can create the files by clicking on the Generate ini files tab. Then select your data and click on generate. The file will apears in ini/OpenSim_MyWorld/[Estate Name].

<br>

<h2>Languages</h2>

<h3>Change the language</h3>

Every one can change the language of the site. To do that, you have to click on the flag on the very top of the page. You would see many flags after and to change the language,
you have to click on the right flag of the language. For example, click on the French flag to see the site in French.

<h3>Translators<h3/>

Some users can be registered as translators. For example, First Admin is a translator. These users have access to the Translations part in the Files Generation section of the menu.
In this part of the site, they can manage the languages available on the site.
<br/>
The translators can be managed in the Translators tab in this section. You can remove a translator with a lower group level and you can remove yourself from the translators.
ATTENTION: it's not recommended to remove all of the translators. If you do that, no one can translate anything. You can also add a translator by select his name in the lower part of the page. Then, click on save and he would be registered as a translator.

<h3>Translations</h3>

You can translate the site in a language only if you are a translator.
<br/>
You can add a language in the New Language tab. To create a language you have to enter a new language name, it's shortname and upload a flag. It's recommended to upload an image with transparent border.
You must upload an image with the jpg format and the image must be 500px wide max.
<br/>
After that, you can translate the expressions of the site by complete the form below. You are not obliged to fill all the sections of the form. If you don't fill the field, the expression will be use.
Then click on Save on the right side of the page.
You can edit a language to add, modify or delete the translation of an expression.

<h3>Translate the site</h3>

To really translate the site, you must export the translation. To do that, go to the Translations tab and click on the Export button near the language you want to export.
After that, everyone can change the language of the site to the exported language with the language menu in the top bar.

<h3>Import jason files</h3>

After create a language, you can import .json file. Your jason file must be placed in resources/lang folder and must be named: shortname.json .
To import this file, click on the Import button near the language. Then, the  translation from the jason file will be automatically upload in the database.
To verify the correct upload, you can click on the Edit button of the language to see if the form is correctly fill.
